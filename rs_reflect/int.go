package rs_reflect

import (
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"math/big"
)

var one = big.NewInt(1)

func init() {
	RT.RegisterType(&Type{
		Type: rs_api.TypeInt,
	})

	RT.RegisterFunc(&Function{
		Signature: FunctionSignature{
			Name:    "<operator_inc>",
			Args:    []rs_api.Type{rs_api.TypeInt},
			Returns: []rs_api.Type{rs_api.TypeInt},
		},
		Invoker: &FuncInvoker{Func: func(env rs_api.Environment) {
			self := env.GetInt()
			value := self.Value
			value = value.Add(value, one)
			self.Value = value
		}},
	})

	RT.RegisterFunc(&Function{
		Signature: FunctionSignature{
			Name:    "<operator_dec>",
			Args:    []rs_api.Type{rs_api.TypeInt},
			Returns: []rs_api.Type{rs_api.TypeInt},
		},
		Invoker: &FuncInvoker{Func: func(env rs_api.Environment) {
			self := env.GetInt()
			value := self.Value
			value = value.Sub(value, one)
			self.Value = value
		}},
	})

	RT.RegisterFunc(&Function{
		Signature: FunctionSignature{
			Name:    "toString",
			Args:    []rs_api.Type{rs_api.TypeInt},
			Returns: []rs_api.Type{rs_api.TypeString},
		},
		Invoker: &FuncInvoker{Func: func(env rs_api.Environment) {
			self := env.GetInt()
			stringValue := rs_api.String{Value: self.String()}
			env.PopInt()
			env.PushString(stringValue)
		}},
	})
}
