package rs_reflect

type Index uint32

func IndexOfString(s string) Index {
	var result Index = 31
	for _, it := range []byte(s) {
		result = result*31 + Index(it)
	}
	return result
}
