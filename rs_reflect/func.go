package rs_reflect

import "gitlab.com/rhinestone-project/go-rsvm/rs_api"

type Invoker interface {
	Invoke(env rs_api.Environment)
}

type FuncInvoker struct {
	Func func(env rs_api.Environment)
}

func (f *FuncInvoker) Invoke(env rs_api.Environment) {
	f.Func(env)
}

type Function struct {
	Signature FunctionSignature
	Invoker   Invoker
	index     Index
}

func (f *Function) Index() Index {
	if f.index == 0 {
		f.index = f.Signature.Index()
	}
	return f.index
}
