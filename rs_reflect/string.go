package rs_reflect

import "gitlab.com/rhinestone-project/go-rsvm/rs_api"

func init() {
	RT.RegisterType(&Type{
		Type: rs_api.TypeString,
	})

	RT.RegisterFunc(&Function{
		Signature: FunctionSignature{
			Name:    "toString",
			Args:    []rs_api.Type{rs_api.TypeString},
			Returns: []rs_api.Type{rs_api.TypeString},
		},
		Invoker: &FuncInvoker{Func: func(env rs_api.Environment) {
			//return self
		}},
	})
}
