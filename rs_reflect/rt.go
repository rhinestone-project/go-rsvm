package rs_reflect

import "gitlab.com/rhinestone-project/go-rsvm/rs_api"

type reflectionTable struct {
	types     []*Type
	functions []*Function
}

func (rt *reflectionTable) RegisterType(t *Type) {
	rt.types = append(rt.types, t)
}

func (rt *reflectionTable) RegisterFunc(f *Function) {
	rt.functions = append(rt.functions, f)
}

func (rt *reflectionTable) FindType(name string) *Type {
	for _, it := range rt.types {
		if it.Type.Name() == name {
			return it
		}
	}
	return nil
}

func (rt *reflectionTable) FindTypeByIndex(i Index) *Type {
	for _, it := range rt.types {
		if it.Index() == i {
			return it
		}
	}
	return nil
}

func (rt *reflectionTable) FindFunc(name string, args, returns []rs_api.Type) *Function {
	for _, it := range rt.functions {
		if it.Signature.Name == name {
			match := true
			for i := 0; i < len(args) && match; i++ {
				if args[i] != it.Signature.Args[i] {
					match = false
				}
			}
			if !match {
				continue
			}
			for i := 0; i < len(returns) && match; i++ {
				if returns[i] != it.Signature.Returns[i] {
					match = false
				}
			}
			if match {
				return it
			}
		}
	}
	return nil
}

func (rt *reflectionTable) FindFuncByIndex(i Index) *Function {
	for _, it := range rt.functions {
		if it.Index() == i {
			return it
		}
	}
	return nil
}

func (rt *reflectionTable) FindFunctions(name string, args []rs_api.Type) []*Function {
	functions := make([]*Function, 0, 1)
	for _, it := range rt.functions {
		if it.Signature.Name == name {
			match := true
			for i := 0; i < len(args) && match; i++ {
				if args[i] != it.Signature.Args[i] {
					match = false
				}
			}
			if match {
				functions = append(functions, it)
			}
		}
	}
	return functions
}

var RT reflectionTable = reflectionTable{
	types:     make([]*Type, 0, 2),
	functions: make([]*Function, 0, 2),
}
