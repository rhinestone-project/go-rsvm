package rs_reflect

import "gitlab.com/rhinestone-project/go-rsvm/rs_api"

type Signature []rs_api.Type

func (s *Signature) Append(t rs_api.Type) *Signature {
	*s = append(*s, t)
	return s
}

func (s *Signature) Len() int {
	return len(*s)
}

func (s *Signature) String() string {
	result := ""
	for _, it := range *s {
		result += it.Signature()
	}
	return result
}

func (s *Signature) Index() Index {
	return IndexOfString(s.String())
}

type FunctionSignature struct {
	Name          string
	Args, Returns Signature
}

func (fs *FunctionSignature) String() string {
	return fs.Name + "(" + fs.Args.String() + ")" + fs.Returns.String()
}

func (fs *FunctionSignature) Index() Index {
	return IndexOfString(fs.String())
}
