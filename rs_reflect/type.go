package rs_reflect

import "gitlab.com/rhinestone-project/go-rsvm/rs_api"

type Type struct {
	Type  rs_api.Type
	index Index
}

func (t *Type) Signature() string {
	return t.Type.Signature()
}

func (t *Type) Index() Index {
	if t.index == 0 {
		t.index = IndexOfString(t.Signature())
	}
	return t.index
}
