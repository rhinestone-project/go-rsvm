module gitlab.com/rhinestone-project/go-rsvm

go 1.14

require (
	gitlab.com/egor9814/go-logger v1.0.5
	gitlab.com/egor9814/go-serialize v1.1.1
)
