package rs_bytecode

var opCodesNames = [...]string{
	Nop:             "Nop",
	LoadConstInt:    "LoadConstInt",
	LoadConstString: "LoadConstString",
	LoadInt:         "LoadInt",
	LoadString:      "LoadString",
	StoreInt:        "StoreInt",
	StoreString:     "StoreString",
	PopInt:          "PopInt",
	PopString:       "PopString",
	DupInt:          "DupInt",
	DupString:       "DupString",
	InvokeNative:    "InvokeNative",
	End:             "End",
}

func (opCode Type) Name() string {
	return opCodesNames[opCode]
}

var opCodesDescriptions = [...]string{
	Nop:             "Nothing to do",
	LoadConstInt:    "Load integer constant to Stack",
	LoadConstString: "Load string constant to Stack",
	LoadInt:         "Load integer to Stack by key",
	LoadString:      "Load string to Stack by key",
	StoreInt:        "Store integer form Stack to locals by key",
	StoreString:     "Store string form Stack to locals by key",
	PopInt:          "Pop integer from Stack",
	PopString:       "Pop string from Stack",
	DupInt:          "Duplicate integer in Stack",
	DupString:       "Duplicate string in Stack",
	InvokeNative:    "Invoke native function",
	End:             "End",
}

func (opCode Type) Desc() string {
	return opCodesDescriptions[opCode]
}
