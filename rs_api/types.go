package rs_api

type Type byte

const (
	TypeVoid Type = iota
	TypeInt
	TypeString
)

var tNames = [...]string{
	TypeVoid:   "void",
	TypeInt:    "int",
	TypeString: "string",
}

func (t Type) Name() string {
	return tNames[t]
}

var tSignatures = [...]string{
	TypeVoid:   "v",
	TypeInt:    "i",
	TypeString: "x",
}

func (t Type) Signature() string {
	return tSignatures[t]
}
