package rs_api

import (
	"errors"
	"math/big"
	"strconv"
)

type Int struct {
	Value *big.Int
}

func NewInt(text string, base int) (*Int, error) {
	bi := new(big.Int)
	runes := make([]rune, len(text))
	idx := 0
	for _, it := range text {
		if it != '_' {
			runes[idx] = it
			idx++
		}
	}
	text = string(runes[:idx])
	if i, ok := bi.SetString(text, base); ok {
		return &Int{Value: i}, nil
	} else {
		return nil, errors.New("cannot cast \"" + text + "\" to integer with base " + strconv.Itoa(base))
	}
}

func IntOf(i *big.Int) *Int {
	return &Int{Value: i}
}

func (i *Int) CompareTo(other *Int) int {
	return i.Value.Cmp(other.Value)
}

func (i *Int) ToInt() int {
	return int(i.Value.Int64())
}

func (i *Int) ToBytes() []byte {
	return i.Value.Bytes()
}

func (i *Int) String() string {
	return i.Value.String()
}

var Zero = IntOf(big.NewInt(0))
