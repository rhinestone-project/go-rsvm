package main

import (
	"fmt"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"gitlab.com/rhinestone-project/go-rsvm/src/bytecode"
	"gitlab.com/rhinestone-project/go-rsvm/src/env"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
	"gitlab.com/rhinestone-project/go-rsvm/src/version"
	"io"
	"os"
	"runtime"
	"strings"
)

func printHelp() {
	fmt.Println("RhineStone Virtual Machine usage:")
	fmt.Println("  rsvm [OPTIONS] <compiled_file>")
	fmt.Println("  OPTIONS:")
	fmt.Println("    -h | --help         print this help")
	fmt.Println("    -v | --version      print RhineStone Virtual Machine version")
	fmt.Println("  <compiled_file>")
	fmt.Println("    path to .rsbin file")
	fmt.Println()
}

func readByteCode(filePath string) *bytecode.Program {
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("open error:", err.Error())
		fmt.Println()
		return nil
	}

	defer func(c io.Closer) {
		if err := c.Close(); err != nil {
			fmt.Println("error while closing file:", err.Error())
			fmt.Println()
		}
	}(file)

	p, err := bytecode.LoadProgram(file)
	if err != nil {
		fmt.Println("cannot load bytecode:", err.Error())
		fmt.Println()
		return nil
	}

	return p
}

func execute(args []string) int {
	var filePath string
	var programArgs []string
L:
	for idx, arg := range args {
		switch arg {
		case "-h", "--help":
			printHelp()
			return 0

		case "-v", "--version":
			fmt.Println("RhineStone Virtual Machine")
			fmt.Println("Version:", version.SemVer)
			fmt.Println("OS Name:", runtime.GOOS)
			fmt.Println("OS Architecture:", runtime.GOARCH)
			fmt.Println()
			return 0

		case "--":
			programArgs = args[idx+1:]
			break L

		default:
			if strings.HasPrefix(arg, "-") {
				fmt.Println("error: unknown flag", arg)
				fmt.Println("type rsvm --help")
				fmt.Println()
				return 0
			} else if len(filePath) == 0 {
				filePath = arg
			} else {
				fmt.Println("error: rsvm not support multiple files")
				fmt.Println()
				return 1
			}
		}
	}

	if len(filePath) == 0 {
		fmt.Println("error: file not specified")
		fmt.Println()
		return 1
	}

	_, err := os.Stat(filePath)
	if err != nil {
		fmt.Println("error: file", filePath, "not found")
		fmt.Println(" ", err.Error())
		fmt.Println()
		return 1
	}

	p := readByteCode(filePath)
	if p == nil {
		return 1
	}

	e := env.New()
	if dumper, ok := e.(interface {
		DumpStack()
	}); ok {
		defer dumper.DumpStack()
	}

	err = p.Execute(e, programArgs)
	if err != nil {
		fmt.Println("execution error:", err.Error())
		fmt.Println()
		return 1
	}

	exitCode := e.GetInt()
	if exitCode != nil {
		if code := exitCode.CompareTo(rs_api.Zero); code != 0 {
			return exitCode.ToInt()
		}
	}

	return 0
}

func main() {
	log.SetStdErr()
	log.D("started rsvm (version: " + version.SemVer + ")")

	exitCode := execute(os.Args[1:])
	if exitCode != 0 {
		os.Exit(exitCode)
	}
}
