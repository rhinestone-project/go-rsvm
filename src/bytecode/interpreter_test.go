package bytecode

import (
	"gitlab.com/rhinestone-project/go-rsvm/rs_bytecode"
	"gitlab.com/rhinestone-project/go-rsvm/src/env"
	"testing"
)

func TestInterpret(t *testing.T) {
	source := []byte{
		13, 12, 9, 18, 26, 7, 26, 28,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 20,
		byte(rs_bytecode.LoadInt),
		0, 0, 0, 4, 't', 'm', 'p', 'i',
		byte(rs_bytecode.InvokeNative),
		231, 189, 105, 95,
		byte(rs_bytecode.InvokeNative),
		6, 128, 196, 123,
		byte(rs_bytecode.End),
	}

	p, err := LoadProgram(source)
	if err != nil {
		t.Error(err)
		return
	}

	e := env.New()

	err = p.Execute(e, []string{})
	if err != nil {
		t.Error(err)
		return
	}

	str := e.GetString()
	if str.String() != "1" {
		t.Errorf("got %q, want \"255\"", str.String())
		return
	}
	e.PopString()
}
