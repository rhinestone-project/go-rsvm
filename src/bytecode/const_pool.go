package bytecode

import (
	go_serialize "gitlab.com/egor9814/go-serialize"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
	"math/big"
)

type constPool struct {
	data []byte
}

func (cp *constPool) intAt(index uint) int {
	result := 0
	for i := uint(0); i < 4; i++ {
		result <<= 8
		result |= int(cp.data[i+index])
	}
	return result
}

func (cp *constPool) bigIntAt(index uint) *big.Int {
	bytesCount := cp.intAt(index)
	start := index + 4
	intBytes := cp.data[start : start+uint(bytesCount)]
	return new(big.Int).SetBytes(intBytes)
}

func (cp *constPool) stringAt(index uint) string {
	bytesCount := cp.intAt(index)
	start := index + 4
	stringBytes := cp.data[start : start+uint(bytesCount)]
	return string(stringBytes)
}

func (cp *constPool) Serialize(_ *go_serialize.Serializer) error {
	panic("for type constPool method Serialize() not supported")
}

func (cp *constPool) Deserialize(in *go_serialize.Deserializer) error {
	log.D("constPool/deserializing...")
	defer func() {
		log.D("constPool/deserialized")
	}()

	l, err := in.Int()
	if err != nil {
		log.E("constPool/" + err.Error())
		return err
	}
	cp.data = make([]byte, l)
	return in.Bytes(cp.data)
}
