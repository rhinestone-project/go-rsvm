package bytecode

import (
	go_serialize "gitlab.com/egor9814/go-serialize"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
)

type HeaderFlagType uint32

const (
	HeaderNoFlags HeaderFlagType = 0

	HeaderTargetV1Flag HeaderFlagType = 1 << iota
)

type Header struct {
	Magic uint64
	Flags HeaderFlagType
}

func (h *Header) Serialize(out *go_serialize.Serializer) error {
	log.D("header/serializing...")
	defer func() {
		log.D("header/serialized")
	}()

	out.MustUInt64(h.Magic)
	out.MustUInt32(uint32(h.Flags))
	return nil
}

func (h *Header) Deserialize(in *go_serialize.Deserializer) error {
	log.D("header/deserializing...")
	defer func() {
		log.D("header/deserialized")
	}()

	in.MustUInt64Ptr(&h.Magic)
	in.MustUInt32Ptr((*uint32)(&h.Flags))
	return nil
}

func (h *Header) IsValid() bool {
	log.D("header/validating...")
	defer func() {
		log.D("header/validated")
	}()

	ser, data := go_serialize.NewByteArraySerializer(8)
	ser.MustUInt64(h.Magic)
	if len(*data) != 8 {
		return false
	}
	for idx, it := range *data {
		(*data)[idx] = byte(^(int(it) - 128))
	}
	return "rsvmexec" == string(*data)
}

func (h *Header) TestFlag(f HeaderFlagType) bool {
	return (h.Flags & f) != 0
}
