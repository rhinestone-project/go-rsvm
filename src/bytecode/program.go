package bytecode

import (
	"errors"
	go_serialize "gitlab.com/egor9814/go-serialize"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
)

type Program struct {
	constPool *constPool
	bytecode  *bytecode
}

func (p *Program) Serialize(_ *go_serialize.Serializer) error {
	panic("for type Program method Serialize() not supported")
}

func (p *Program) Deserialize(in *go_serialize.Deserializer) error {
	log.D("program/deserializing...")
	defer func() {
		log.D("program/deserialized")
	}()

	if err := p.constPool.Deserialize(in); err != nil {
		log.E("program/" + err.Error())
		return err
	}
	return p.bytecode.Deserialize(in)
}

func (p *Program) Execute(env rs_api.Environment, args []string) (err error) {
	log.D("program/executing...")
	defer func() {
		log.D("program/executed")
	}()

	if len(args) > 0 {
		return errors.New("arguments not support yet")
	}

	i := interpreter{
		cp:       p.constPool,
		bytecode: p.bytecode.reader(),
	}
	return i.execute(env)
}
