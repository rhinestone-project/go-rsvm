package bytecode

import (
	"errors"
	"fmt"
	go_serialize "gitlab.com/egor9814/go-serialize"
	"gitlab.com/rhinestone-project/go-rsvm/rs_bytecode"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
	"io"
	"io/ioutil"
	"strconv"
)

func readBytes(source interface{}) ([]byte, error) {
	log.D("loader/reading binary code...")
	switch s := source.(type) {
	case io.Reader:
		return ioutil.ReadAll(s)
	case []byte:
		return s, nil
	case *[]byte:
		return *s, nil
	default:
		return nil, fmt.Errorf("unsupported source type %T", s)
	}
}

func validateByteCode(bc *bytecode) error {
	log.D("loader/validating bytecode...")
	r := bc.reader()
	for {
		rawCode, err := r.Byte()
		if err != nil {
			return err
		}

		code := rs_bytecode.Type(rawCode)

		switch code {
		case rs_bytecode.End:
			return nil

		case rs_bytecode.LoadConstInt, rs_bytecode.LoadConstString:
			if _, err := r.UInt(); err != nil {
				return errors.New("cannot read const index: " + err.Error())
			}

		case rs_bytecode.LoadInt, rs_bytecode.LoadString, rs_bytecode.StoreInt, rs_bytecode.StoreString:
			if _, err := r.String(); err != nil {
				return errors.New("cannot read string: " + err.Error())
			}

		case rs_bytecode.Nop,
			rs_bytecode.PopInt, rs_bytecode.PopString,
			rs_bytecode.DupInt, rs_bytecode.DupString:
			break

		case rs_bytecode.InvokeNative:
			if _, err := r.UInt32(); err != nil {
				return errors.New("cannot read native index: " + err.Error())
			}

		default:
			return errors.New("invalid opcode " + strconv.Itoa(int(code)))
		}
	}
}

func LoadProgram(source interface{}) (*Program, error) {
	log.D("loader/loading program...")
	defer func() {
		log.D("loader/program loaded")
	}()

	programBytes, err := readBytes(source)
	if err != nil {
		log.E("loader/" + err.Error())
		return nil, err
	}
	log.D("loader/binary code readed")

	deser := go_serialize.NewByteArrayDeserializer(programBytes)

	header := &Header{}
	if err := deser.Serializable(header); err != nil {
		log.E("loader/" + err.Error())
		return nil, err
	}

	if !header.IsValid() {
		return nil, errors.New("invalid rsbin magic code")
	}

	p := &Program{
		constPool: &constPool{},
		bytecode:  &bytecode{},
	}
	if err := deser.Serializable(p); err != nil {
		log.E("loader/" + err.Error())
		return nil, err
	}

	if err := validateByteCode(p.bytecode); err != nil {
		log.E("loader/" + err.Error())
		return nil, err
	}

	return p, nil
}
