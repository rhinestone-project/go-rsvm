package bytecode

import (
	go_serialize "gitlab.com/egor9814/go-serialize"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
)

type bytecode struct {
	data []byte
}

func (bc *bytecode) reader() *go_serialize.Deserializer {
	return go_serialize.NewByteArrayDeserializer(bc.data)
}

func (bc *bytecode) Serialize(_ *go_serialize.Serializer) error {
	panic("for type bytecode method Serialize() not supported")
}

func (bc *bytecode) Deserialize(in *go_serialize.Deserializer) error {
	log.D("bytecode/deserializing...")
	defer func() {
		log.D("bytecode/deserialized")
	}()

	l, err := in.Int()
	if err != nil {
		log.E("bytecode/" + err.Error())
		return err
	}
	bc.data = make([]byte, l)
	return in.Bytes(bc.data)
}
