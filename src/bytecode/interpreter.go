package bytecode

import (
	"errors"
	go_serialize "gitlab.com/egor9814/go-serialize"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"gitlab.com/rhinestone-project/go-rsvm/rs_bytecode"
	"gitlab.com/rhinestone-project/go-rsvm/rs_reflect"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
	"math/big"
	"strconv"
)

type interpreter struct {
	cp       *constPool
	bytecode *go_serialize.Deserializer
}

func (i *interpreter) readBytes(out []byte) error {
	return i.bytecode.Bytes(out)
}

func (i *interpreter) readUInt() (uint, error) {
	return i.bytecode.UInt()
}

func (i *interpreter) readInt() (int, error) {
	return i.bytecode.Int()
}

func (i *interpreter) readUInt32() (uint32, error) {
	return i.bytecode.UInt32()
}

func (i *interpreter) readIndex() (rs_reflect.Index, error) {
	if ui, err := i.readUInt32(); err != nil {
		return 0, err
	} else {
		return rs_reflect.Index(ui), nil
	}
}

func (i *interpreter) readBigInt() (*big.Int, error) {
	count, err := i.readInt()
	if err != nil {
		return nil, err
	}
	bytes := make([]byte, count)
	err = i.readBytes(bytes)
	if err != nil {
		return nil, err
	}
	result := new(big.Int)
	result.SetBytes(bytes)
	return result, nil
}

func (i *interpreter) readString() (string, error) {
	return i.bytecode.String()
}

func (i *interpreter) readOpCode() (rs_bytecode.Type, error) {
	val, err := i.bytecode.Byte()
	if err != nil {
		return 0, err
	}
	return rs_bytecode.Type(val), nil
}

func (i *interpreter) execute(env rs_api.Environment) error {
	/// TODO: in feature delete this @{
	tmpi := rs_api.IntOf(big.NewInt(0))
	tmps := rs_api.StringOf("")
	/// TODO: }@

	for {
		code, err := i.readOpCode()
		if err != nil {
			return err
		}

		log.D("interpreter/opcode " + code.Name())

		switch code {
		case rs_bytecode.End:
			return nil

		case rs_bytecode.Nop:
			// nop

		case rs_bytecode.LoadConstInt:
			index, err := i.readUInt()
			if err != nil {
				return errors.New("cannot read const index: " + err.Error())
			}
			integer := i.cp.bigIntAt(index)
			env.PushInt(*rs_api.IntOf(integer))

		case rs_bytecode.LoadConstString:
			index, err := i.readUInt()
			if err != nil {
				return errors.New("cannot read const index: " + err.Error())
			}
			str := i.cp.stringAt(index)
			env.PushString(*rs_api.StringOf(str))

		case rs_bytecode.LoadInt:
			key, err := i.readString()
			if err != nil {
				return errors.New("cannot read string: " + err.Error())
			} else if key == "tmpi" {
				env.PushInt(*tmpi)
			} else {
				return errors.New("cannot load int with key \"" + key + "\"")
			}

		case rs_bytecode.LoadString:
			key, err := i.readString()
			if err != nil {
				return errors.New("cannot read string: " + err.Error())
			} else if key == "tmps" {
				env.PushString(*tmps)
			} else {
				return errors.New("cannot load string with key \"" + key + "\"")
			}

		case rs_bytecode.StoreInt:
			key, err := i.readString()
			if err != nil {
				return errors.New("cannot read string: " + err.Error())
			} else if key == "tmpi" {
				integer := env.GetInt()
				*tmpi = *integer
				env.PopInt()
			} else {
				return errors.New("cannot store int with key \"" + key + "\"")
			}

		case rs_bytecode.StoreString:
			key, err := i.readString()
			if err != nil {
				return errors.New("cannot read string: " + err.Error())
			} else if key == "tmps" {
				str := env.GetString()
				*tmps = *str
				env.PopString()
			} else {
				return errors.New("cannot store string with key \"" + key + "\"")
			}

		case rs_bytecode.PopInt:
			env.PopInt()

		case rs_bytecode.PopString:
			env.PopString()

		case rs_bytecode.DupInt:
			integer := *env.GetInt()
			env.PushInt(integer)

		case rs_bytecode.DupString:
			str := *env.GetString()
			env.PushString(str)

		case rs_bytecode.InvokeNative:
			index, err := i.readIndex()
			if err != nil {
				return errors.New("cannot read native index: " + err.Error())
			} else {
				f := rs_reflect.RT.FindFuncByIndex(index)
				if f == nil {
					return errors.New("native function ::0x" +
						strconv.FormatUint(uint64(index), 16) + " not found")
				} else {
					f.Invoker.Invoke(env)
				}
			}

		default:
			return errors.New("unsupported opcode " + strconv.Itoa(int(code)))
		}
	}
}
