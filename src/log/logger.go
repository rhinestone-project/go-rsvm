package log

import (
	golog "gitlab.com/egor9814/go-logger"
	"gitlab.com/rhinestone-project/go-rsvm/src/version"
	"io"
)

var logger = golog.New("rsvm", version.Debug)

func SetOutput(w io.Writer) {
	logger.SetOutputWriter(w)
}

func SetStdOut() {
	logger.Output = golog.STDOUT
}

func SetStdErr() {
	logger.Output = golog.STDERR
}

func E(message string) {
	logger.E(message)
}

func F(message string) {
	logger.F(message)
}

func W(message string) {
	logger.W(message)
}

func I(message string) {
	logger.I(message)
}

func D(message string) {
	logger.D(message)
}
