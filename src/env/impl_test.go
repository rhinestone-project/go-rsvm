package env

import (
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"gitlab.com/rhinestone-project/go-rsvm/rs_reflect"
	"testing"
)

func TestIntToString(t *testing.T) {
	env := New()
	i, err := rs_api.NewInt("fe", 16)
	if err != nil {
		t.Error(err)
		return
	}
	env.PushInt(*i)

	f := rs_reflect.RT.FindFunc(
		"<operator_inc>", []rs_api.Type{rs_api.TypeInt}, []rs_api.Type{rs_api.TypeInt})
	if f == nil {
		t.Error("int reflection not loaded")
		return
	}
	f.Invoker.Invoke(env)

	f = rs_reflect.RT.FindFunc("toString", []rs_api.Type{rs_api.TypeInt}, []rs_api.Type{rs_api.TypeString})
	if f == nil {
		t.Error("int reflection not loaded")
		return
	}
	f.Invoker.Invoke(env)

	s := env.GetString()
	if s.Value != "255" {
		t.Error("toString(int):(string) -> incorrect behaviour")
		return
	}
}
