package env

import (
	"fmt"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
)

type intPtr struct {
	val rs_api.Int
}

type stringPtr struct {
	val rs_api.String
}

type operand struct {
	i    *intPtr
	s    *stringPtr
	next *operand
}

type operandStack struct {
	head *operand
}

func (os *operandStack) pushInt(i rs_api.Int) {
	os.head = &operand{
		i:    &intPtr{val: i},
		s:    nil,
		next: os.head,
	}
}

func (os *operandStack) pushString(s rs_api.String) {
	os.head = &operand{
		i:    nil,
		s:    &stringPtr{val: s},
		next: os.head,
	}
}

func (os *operandStack) pop() {
	os.head = os.head.next
}

func (os *operandStack) getInt() *rs_api.Int {
	if os.head == nil {
		return nil
	}
	ptr := os.head.i
	return &ptr.val
}

func (os *operandStack) getString() *rs_api.String {
	if os.head == nil {
		return nil
	}
	ptr := os.head.s
	return &ptr.val
}

func (os *operandStack) dump() {
	if os.head == nil {
		return
	}
	result := fmt.Sprintln("<opstack>")
	for it := os.head; it != nil; it = it.next {
		if it.s != nil {
			result += fmt.Sprintf("  <string>%s</string>\n", it.s.val.Value)
		} else if it.i != nil {
			result += fmt.Sprintf("  <int>%s</int>\n", it.i.val.String())
		} else {
			result += fmt.Sprintf("  <null/>\n")
		}
	}
	result += fmt.Sprintln("</opstack>")
	log.D(result)
}
