package env

import (
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"gitlab.com/rhinestone-project/go-rsvm/src/log"
)

type envImpl struct {
	stack operandStack
}

func New() rs_api.Environment {
	return &envImpl{}
}

func (env *envImpl) PushInt(i rs_api.Int) {
	log.D("env/push int \"" + i.String() + "\"")
	env.stack.pushInt(i)
}

func (env *envImpl) PushString(s rs_api.String) {
	log.D("env/push string \"" + s.String() + "\"")
	env.stack.pushString(s)
}

func (env *envImpl) PopInt() {
	log.D("env/pop int")
	env.stack.pop()
}

func (env *envImpl) PopString() {
	log.D("env/pop string")
	env.stack.pop()
}

func (env *envImpl) GetInt() *rs_api.Int {
	log.D("env/get int")
	return env.stack.getInt()
}

func (env *envImpl) GetString() *rs_api.String {
	log.D("env/get string")
	return env.stack.getString()
}

func (env *envImpl) DumpStack() {
	log.D("env/dumping opstack")
	env.stack.dump()
}
