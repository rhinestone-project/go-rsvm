package version

import "strconv"

const (
	Debug = debug

	Major = 0
	Minor = 3
	Build = buildNumber

	Prefix = "primary-"
)

var (
	Postfix string
	SemVer  string
)

func init() {
	if Debug {
		Postfix += "-DEBUG"
	}

	SemVer = Prefix +
		strconv.Itoa(Major) + "." +
		strconv.Itoa(Minor) + "." +
		strconv.Itoa(Build) +
		Postfix
}
