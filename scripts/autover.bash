#!/usr/bin/env bash

if [ ! -d $GOPATH ]; then
  read GOPATH <<< `go env GOPATH`
  if [ ! -d $GOPATH ]; then
    echo "cannot resolve GOPATH"
    exit 1
  fi
fi

repo_name=gitlab.com/rhinestone-project/go-rs-tools
tools=$GOPATH/src/$repo_name
if [ ! -d $tools ]; then
  git clone https://$repo_name.git $tools
  if [ ! -d $tools ]; then
    echo "cannot get go-rs-tools"
    exit 1
  fi
  _cd=`pwd`
  cd $tools
  go install
  cd $_cd
fi

go-rstool autover -f $GOPATH/src/gitlab.com/rhinestone-project/go-rsvm/src/version/build_number.go
