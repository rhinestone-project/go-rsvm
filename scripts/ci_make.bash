#!/usr/bin/env bash

cd $CI_PROJECT_DIR
mkdir -p artifacts

artifact=rsvm
if [ "$1" == "windows" ]; then
  artifact=$artifact.exe
fi

out=.build
mkdir -p $out

GOOS=$1 GOARCH=$2 go build -ldflags "-extldflags '-static'" -o $out/$artifact -tags release
if (( $? != 0 )); then
  echo "make failed!"
  exit 1
else
  echo "make artifact..."
  archive=artifacts/rsbuild-$1-$2
  if [ "$1" == "windows" ]; then
    zip -9 $archive.zip -j $out/$artifact
  else
    tar -cf $archive.tar -C $out $artifact
    gzip $archive.tar
  fi
fi
